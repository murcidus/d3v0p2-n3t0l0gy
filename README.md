## Домашнее задание 2

В каталоге terraform и его подкаталогах будут проигнорированы файлы:  
crash.log  
override.tf  
override.tf.json  
.terraformrc  
terraform.rc  

В каталоге terraform и его подкаталогах будут проигнорированы файлы по маске:  
`*`.tfstate  
`*`.tfstate.`*`  
`*`.tfvars  
`*`_override.tf  
`*`_override.tf.json  

В каталоге terraform будут проигнорированы все файлы из подкаталогов с именем `.terraform`
